# README #

# Loopit coding test

## Features

- User login
- JWToken for API authentication
- Search by model name

## Dependecies
- react-router-dom
- react-loader-spinner
- axios

# Install & run
```sh
$ npm install
$ npm start
```

# Credentials

- email: joe@example.com
- password: abc123