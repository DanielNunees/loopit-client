import axios from 'axios';
import config from '../../app.config.json'
import localStorageService from '../storage/localstorage';
import { history } from '../..';

// Add a request interceptor
axios.interceptors.request.use(function (config) {
   let token = localStorageService.get('_token');
   if (token === undefined) history.push('/login');
   config.headers.Authorization = `Bearer ${token}`;
   return config;
}, function (error) {
   // Do something with request error
   return Promise.reject(error);
});


// Add a response interceptor
axios.interceptors.response.use(function (response) {
   // Any status code that lie within the range of 2xx cause this function to trigger
   // Do something with response data
   return response;
}, function (error) {
   // Any status codes that falls outside the range of 2xx cause this function to trigger
   // Do something with response error
   if (error?.response?.status === 401) {
      localStorageService.remove('_token');
      history.push('/login');
   }
   return Promise.reject(error);
});


const apiService = {

   get: async (url) => {
      const promise = new Promise((resolve, reject) => {
         setTimeout(async () => {
            try {
               let response = await axios.get(config.SERVER_URL + url);
               resolve(response.data.data);
            } catch (err) {
               let error = { 'id': 1, 'error': true, message: err };
               resolve(error); // TypeError: failed to fetch
            }
         }, 1500);
      });
      return promise;
   },
   post: async (url, data, delay) => {
      const promise = new Promise((resolve, reject) => {
         setTimeout(async () => {
            try {
               let response = await axios.post(config.SERVER_URL + url, data);
               resolve(response.data.data);
            } catch (err) {
               let error = { 'id': 1, 'error': true, message: err };
               resolve(error); // TypeError: failed to fetch
            }
         }, delay);
      });
      return promise;
   },
}



export default apiService;