
const localStorageService = {
   set: (data) => {
      localStorage.setItem('_token', data);
   },
   get: () => {
      return localStorage.getItem('_token');
   },
   remove: (item) => {
      localStorage.removeItem(item);
   }
}



export default localStorageService;