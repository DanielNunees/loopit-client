
import apiService from '../../_helpers/api/api-request';
import localStorageService from '../../_helpers/storage/localstorage';
import { history } from '../..';

export const authenticationService = {
   login,
   logout,
   isAuth,
};

async function login(userData) {
   const response = await apiService.post('auth/login', userData, 1500);
   if (response?.original?.access_token) {
      localStorageService.set(response.original.access_token);
      // redirect to home if already logged in
      history.push('/cars');
   }
   return response;
}

function isAuth() {
   return localStorageService.get('_token') === null ? false : true;
}

async function logout() {
   // remove user from local storage to log user out
   await apiService.post('auth/logout',null,0);
   localStorageService.remove('_token');
   history.push('/login');
}