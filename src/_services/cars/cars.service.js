import apiService from '../../_helpers/api/api-request';

export const carsService = {
   getAll
};

async function getAll() {
   return await apiService.get('cars')
}