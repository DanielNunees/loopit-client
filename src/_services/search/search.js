import apiService from '../../_helpers/api/api-request';

export const searchService = {
   search
};

async function search(title) {
   return await apiService.post('cars/search', title)
}