import './App.css';
import Cars from './components/cars/Cars';
import Login from './components/login/login';
import { Route, Switch } from 'react-router-dom';
import NotFound from './components/errors/NotFound';
import { PrivateRoute } from './components/private/PrivateRoute';


const App = () => {
   return (
      <>
         <Switch>
            <Route path="/login" exact component={Login} />
            <PrivateRoute exact path="/cars" component={Cars} />
            <Route path="*" exact component={NotFound} />
         </Switch>
      </>
   );
}

export default App;
