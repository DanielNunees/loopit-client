import React, { useState } from 'react';
import styles from './navbar.module.css';
import logo from '../../assests/img/logo.json'
import DesktopNavbar from './desktop/desktop.navbar';
import MobileNavbar from './mobile/mobile.navbar';

// Main component
// This component will set the initial data in the grid or list view.
function Navbar() {
   const [mobileBar, setMobileBar] = useState(false);
   const handleToogleBar = () => {
      setMobileBar(!mobileBar);
   }

   return (
      <div className={styles.borderBox} >
         <div className={styles.navbar}>
            <img src={logo.logo} width="90%, 100%" alt="logo" height="auto" />
            <MobileNavbar toogleBar={handleToogleBar} actMobileNav={mobileBar} />
            <DesktopNavbar />
         </div>

      </div>
   );
}

export default Navbar;
