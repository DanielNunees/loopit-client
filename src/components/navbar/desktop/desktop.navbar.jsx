import React from 'react';
import styles from './desktop-navbar.module.css';
import NavbarLink from '../../layout/NavbarLink';
import { authenticationService } from '../../../_services/auth/authentication.service';
import NavbarButton from '../../layout/NavbarButton';

const DesktopNavbar = () => {

   const handleLogout = () => {
      authenticationService.logout();
   }

   return (
      <div className={styles.desktopBarBox}>
         <nav className={styles.desktopNavbar}>
            <NavbarLink to="/cars">Home</NavbarLink>
            <NavbarButton onClick={() => handleLogout()} >Logout</NavbarButton>
         </nav>
      </div>
   );
}

export default DesktopNavbar;