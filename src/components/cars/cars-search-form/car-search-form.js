import React, { useContext, useState } from 'react';
import styles from './cars-search-form.module.css'
import LoadingContext from '../../loader/loading-context';
import { searchService } from '../../../_services/search/search';

//Component responsible for getting the user input for the car search
function JobSearchForm({ searchResultsCallback }) {
   // const [searchResults, setSearchResults] = useState(props);
   const [title, setTitle] = useState("");
   const [typingTimer, setTypingTimer] = useState(0);
   const { setLoading } = useContext(LoadingContext);


   // Set the title state and timeout to execute function
   const handleChange = (evt) => {
      clearTimeout(typingTimer);
      setTitle(evt.target.value);
      const timeoutId = setTimeout(() => { // Timeout set 
         searchCar(evt.target.value);      // to simulate a
      }, 1000);                            // real connection
      setTypingTimer(timeoutId);

   }
   // Send a request to the server, with query string
   // Trying to find a car that matches with the model
   // If the user send a empty string, will retrieve all the cars
   const searchCar = async (model) => {
      searchResultsCallback([]); //clean all the previous cars 
      setLoading(true); // enable loading animation
      const cars = await searchService.search({ model: model });
      searchResultsCallback(cars);
      setLoading(false);
   }

   return (
      <div className={`${styles.form__group} ${styles.field}`}>
         <input className={styles.form__field} type="input" placeholder="Search car Model.." name="search2" value={title} onChange={handleChange} />
         <label htmlFor="name" className={styles.form__label}>Search car Model..</label>
      </div>
   );
}

export default JobSearchForm;