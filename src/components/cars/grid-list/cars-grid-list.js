import React, { useState, useEffect, useCallback, useContext } from 'react';
import MyLoader from '../../loader/loader';
import LoadingContext from '../../loader/loading-context';
import CarsCard from '../cars-card/cars-card';
import JobSearchForm from '../cars-search-form/car-search-form';
import GridListButtons from './grid-list-buttons';
import styles from './grid-list.module.css';


const CarsGridList = ({ cars }) => {
   const [view, setView] = useState("grid");
   const [dataDisplayed, setDataDisplayed] = useState([]);
   const { loading } = useContext(LoadingContext);

   useEffect(() => {
      setDataDisplayed(cars);
   }, [cars]);

   // Update the results from the search
   const handleSearchResult = useCallback(
      (searchResults) => {
         setDataDisplayed(searchResults);
      }, [],
   );

   const handleSetView = (view) => {
      setView(view);
   }

   const evalueteResponse = () => {
      if (loading === false) {
         if ((dataDisplayed === undefined ||
            dataDisplayed.length === 0 ||
            dataDisplayed.error === true)
         ) {
            return displayErrorMessage(dataDisplayed);
         }
         return displayData();
      }

   }

   const displayData = () => {
      return (
         <div className={`${view === "grid" ? styles.grid : styles.list}`}>
            <div className={styles.row}>
               {dataDisplayed.map((car) => (
                  <CarsCard key={car.id} car={car} />
               ))}
            </div>
         </div >
      );
   }

   const displayErrorMessage = (data) => {
      if (data !== undefined && data.message) {
         return (<h2 className={styles.notFoundText} >{data.message.message}</h2>);
      }
      return (<h2 className={styles.notFoundText} >Nothing Found</h2>);
   }

   return (
      <div className={styles.container}>
         <div className={styles.options}>
            <GridListButtons setViewCallback={handleSetView} view={view} />
            <JobSearchForm searchResultsCallback={handleSearchResult} />
         </div>
         <MyLoader loading={loading} />
         {evalueteResponse()}
      </div>
   );
}

export default CarsGridList;