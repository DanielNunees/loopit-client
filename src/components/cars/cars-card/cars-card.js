import React from 'react';
import cardStyle from './cars-card.module.css';

// Card car component
// Show the data of one car at the time
function CarsCard({ car, error }) {
   const formatDate = () => {
      return new Date(car.created_at).toLocaleDateString();
   }
   return (
      <>
         {error === true ? "Server Error (500)" :
            <div className={`${cardStyle.orange} ${cardStyle.cardSplit} ${cardStyle.card}`}>
               <div>
                  <h2>{car.model}</h2>
                  <p className={cardStyle.price}>Price: ${car.price}</p>
                  <p>Maker: {car.maker}</p>
                  <p>Color: {car.color}</p>
                  <p>Year: {car.year}</p>
                  <p>Date Posted: {formatDate()}</p>
               </div>
            </div>
         }
      </>
   );
}

export default CarsCard;