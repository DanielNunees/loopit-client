import React, { useState, useEffect, useMemo } from 'react';
import LoadingContext from '../loader/loading-context';
import CarsGridList from './grid-list/cars-grid-list';
import { carsService } from '../../_services/cars/cars.service';

// Main component
// This component will set the initial data in the grid or list view.
function Cars() {
   const [cars, setCars] = useState([]);
   const [loading, setLoading] = useState(false);
   const providerValue = useMemo(() => ({ loading, setLoading }), [loading, setLoading]);

   useEffect(() => {
      // Using the async await function
      const fetchData = async () => {
         setLoading(true);
         const cars = await carsService.getAll()
         setLoading(false);
         setCars(cars);
      }
      fetchData();
      // Update the grid using the data from the api
   }, []);

   return (
      <LoadingContext.Provider value={providerValue}>
         <CarsGridList cars={cars} />
      </LoadingContext.Provider>
   );
}

export default Cars;