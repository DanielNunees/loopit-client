import React from 'react'

const NavbarButton = (props) => {
   return (
      <button {...props}>{props.children}</button>
   )
}

export default NavbarButton
