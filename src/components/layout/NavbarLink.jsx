import React from 'react'
import { NavLink } from 'react-router-dom';
import styles from './navbarlink.module.css';

const NavbarLink = (props) => {
   return (
      <NavLink {...props} activeClassName={styles.active} >{props.children}</NavLink>
   )
}

export default NavbarLink
