import React, { useContext, useState } from 'react'
import styles from './loginForm.module.css';
import PropTypes from 'prop-types';
import MyLoader from '../../loader/loader';
import LoadingContext from '../../loader/loading-context';
import Unauthenticated from '../../errors/Unauthenticated';

const LoginForm = ({ handleLogin, authenticated }) => {
   const [email, setEmail] = useState();
   const [password, setPassword] = useState();
   const { loading } = useContext(LoadingContext);

   const handleSubmit = e => {
      e.preventDefault();
      handleLogin({
         email,
         password
      });
   }
   return (
      <div className={styles.login}>
         <>
            <figure className={styles.avatar}>
               <img src="//picsum.photos/70" alt="Avatar" className={styles.avatar__image} />
               <figcaption className={styles.avatar__name}>John</figcaption>
            </figure>
            {authenticated === false ? <Unauthenticated /> : ''}
            <form className={styles.form} onSubmit={handleSubmit}>
               <div className={styles.input}>
                  <label className={styles.input__label}>Email</label>
                  <input type="email"
                     className={styles.input__email}
                     placeholder="me@mail.com"
                     onChange={e => setEmail(e.target.value)} />
               </div>
               <div className={styles.input}>
                  <label className={styles.input__label}>Password</label>
                  <input type="password"
                     className={styles.input__password}
                     placeholder="····"
                     onChange={e => setPassword(e.target.value)} />
               </div>
               {loading ? <MyLoader loading={loading} /> :
                  <button className={styles.form__button} type="submit">Log in</button>
               }
            </form>
         </>



      </div>
   )
}

LoginForm.propTypes = {
   handleLogin: PropTypes.func.isRequired
}

export default LoginForm
