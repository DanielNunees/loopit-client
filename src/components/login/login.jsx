import React, { useEffect, useMemo, useState } from 'react'
import { authenticationService } from '../../_services/auth/authentication.service';
import LoadingContext from '../loader/loading-context';
import LoginForm from './form/LoginForm';
import styles from './login.module.css';
import { history } from '../..';

const Login = () => {
   const [loading, setLoading] = useState(false);
   const [authenticated, setAuthenticated] = useState(null);
   const providerValue = useMemo(() => ({ loading, setLoading }), [loading, setLoading]);

   useEffect(() => {
      const currentUser = authenticationService.isAuth();
      if (currentUser) history.push('cars');
   }, [])

   const handleLogin = async (evt) => {
      setLoading(true);
      const fetchData = async () => {
         const response = await authenticationService.login(evt);
         if (response?.error === true) {
            console.log(response);
            setAuthenticated(false);
            setLoading(false);
            return;
         }
      }
      fetchData();
   }

   return (
      <LoadingContext.Provider value={providerValue}>
         <div className={styles.loginWrapper}>
            <LoginForm handleLogin={handleLogin} authenticated={authenticated} />
         </div>
      </LoadingContext.Provider>

   )
}

export default Login
