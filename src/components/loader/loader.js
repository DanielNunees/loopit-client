import React from 'react';
import Loader from "react-loader-spinner";
import styles from './loader.module.css';


function MyLoader(props) {
   return (
      <div style={{ margin: 'auto', textAlign:'center' }} className={props.loading ? '' : styles.hide } >
         <Loader
            type="ThreeDots"
            color="#3432ff"
            height={100}
            width={100}
            visible={props.loading}
         />
      </div>
   )
}

export default MyLoader;