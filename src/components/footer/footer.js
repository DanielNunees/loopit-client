import React from 'react';
import styles from './footer.module.css';
import footerLogo from '../../assests/img/footer-logo.json'


const Footer = () => {
   return (
      <footer className={styles.container}>
         <div className={styles.footer}>
            <div className={styles.boxContainer}>
               <div className={styles.column1}>
                  <div className={styles.footerLogo}>
                     <img src={footerLogo['footer-logo']} alt="footer-logo"></img>
                  </div>
                  <div className={styles.footerText}>
                     <div>Loopit is an end-to-end software solution that allows you to power future mobility services in your business for your own customers such as car subscription</div>
                     <div>© Copyright 2021 Loopit</div>
                  </div>
               </div>
               <div className={styles.footerMenu}>
                  <div className={styles.column2}>
                     <button>Home</button>
                  </div>
                  <div className={styles.column3}>
                     <button>Cars</button>
                  </div>
               </div>
            </div>
         </div>
      </footer>
   );


}

export default Footer;